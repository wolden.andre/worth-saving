export const validateBarnInfo = (barnInfo: BarnInfo): Either<FeiloppsummeringFeil[], ValidBarnInfo> => {
    const ea: Either<FeiloppsummeringFeil, ISODateString> = barnInfo.fodselsdato.value;
    const eb: Either<FeiloppsummeringFeil, boolean> = barnInfo.ekstraOmsorgsdager.value;
    const ec: Either<FeiloppsummeringFeil, boolean> = barnInfo.borSammen.value;
    const ed: Either<FeiloppsummeringFeil, boolean> = barnInfo.aleneOmOmsorgen.value;

    const result: Either<FeiloppsummeringFeil, Tull> = pipe(
        ea,
        map((d: ISODateString) => ({ fodselsdato: d })),
        chain((a) => map((b: boolean) => ({ ...a, ekstraOmsorgsdager: b }))(eb)),
        chain((a) => map((b: boolean) => ({ ...a, borSammen: b }))(ec)),
        chain((a) => map((b: boolean) => ({ ...a, aleneOmOmsorgen: b }))(ed)),
        chain((a) => map((b: string) => ({ ...a, id: b }))(right(barnInfo.id)))
    );

    //
    // Add(addA, ea)
    //     .andAdd(addB, eb)
    //     .andAdd(addC, ec)
    //     .andAdd(addD, ed)

    // pipe(
    //     ea,
    //     fold(
    //         ae => {
    //             return pipe(
    //                 eb,
    //                 fold(
    //                     be => {
    //
    //                     },
    //
    //                 )
    //             )
    //         },
    //         a => {
    //             return pipe(
    //                 eb,
    //                 fold(
    //                     be => {
    //                         return pipe(
    //                             ec,
    //                             fold(
    //                                 ce => {
    //                                     return pipe(
    //                                         ed,
    //                                         fold(
    //                                             de => [be, ce, de],
    //                                             () => [be, ce]
    //                                         )
    //                                     )
    //                                 },
    //                                 c => {
    //                                     return pipe(
    //                                         ed,
    //                                         fold(
    //                                             de => [be, de],
    //                                             () => [be]
    //                                         )
    //                                     )
    //                                 }
    //                             )
    //                         )
    //                     },
    //                     b => {
    //                         return pipe(
    //                             ec,
    //                             fold(
    //                                 ce => {
    //                                     return pipe(
    //                                         ed,
    //                                         fold(
    //                                             (de) => [ce, de],
    //                                             _ => [ce]
    //                                         )
    //                                     )
    //                                 },
    //                                 c => {
    //                                     return pipe(
    //                                         ed,
    //                                         fold(
    //                                             de => [de],
    //                                             d => {
    //                                                 const validBarnInfo: ValidBarnInfo = {
    //                                                     id: barnInfo.id,
    //                                                     fodselsdato: {...barnInfo.fodselsdato, value: a},
    //                                                     ekstraOmsorgsdager: {...barnInfo.ekstraOmsorgsdager, value: b},
    //                                                     borSammen: {...barnInfo.borSammen, value: c},
    //                                                     aleneOmOmsorgen: {...barnInfo.aleneOmOmsorgen, value: d}
    //                                                 };
    //                                                 return validBarnInfo;
    //                                             }
    //                                         )
    //                                     )
    //                                 }
    //                             )
    //                         )
    //                     }
    //                 )
    //             )
    //         }
    //     )
    // )

};
